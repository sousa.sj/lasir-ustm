package ustm.lasir2017.Exercicio;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ustm.lasir2017.R;

public class ExercicioActivity extends AppCompatActivity {
    EditText num1,num2;
    int n1,n2;
    TextView resultado;
    private Button adiccao;
    private Button divisao;
    private Button subtracao;
    private Button multiplicacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercicio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        num1=(EditText) findViewById(R.id.etPrimeiro);
        num2=(EditText) findViewById(R.id.etSegundo);
        resultado=(TextView) findViewById(R.id.tvResultado);
        adiccao=(Button) findViewById(R.id.btAdicionar);

        adiccao.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    //Conversao de String para Inteiro
                    n1=Integer.parseInt(num1.getText().toString());
                    n2=Integer.parseInt(num2.getText().toString());
                    double n3= n1+n2;
                    resultado.setText("A soma de "+n1+" + "+n2+" = "+n3);

                }catch (Exception e){

                }

            }
        });
        divisao=(Button) findViewById(R.id.btDividir);
        divisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    n1=Integer.parseInt(num1.getText().toString());
                    n2=Integer.parseInt(num2.getText().toString());
                    double n3= n1/n2;
                    resultado.setText("A Divisao de "+n1+"+" +n2+" é "+n3);

                }catch (Exception e){

                }

            }
        });

        subtracao=(Button) findViewById(R.id.btDividir);
        subtracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    n1=Integer.parseInt(num1.getText().toString());
                    n2=Integer.parseInt(num2.getText().toString());
                    double n3= n1-n2;
                    resultado.setText("A subtracao de"+n1+"+"+n2+"="+n3);
                }catch (Exception e){

                }
            }
        });
        multiplicacao=(Button) findViewById(R.id.btMultiplicar);
        multiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    n1=Integer.parseInt(num1.getText().toString());
                    n2=Integer.parseInt(num2.getText().toString());
                    double n3= n1*n2;
                    resultado.setText("A subtracao de "+n1+" + "+n2+" = "+n3);


                }catch (Exception e){

                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
