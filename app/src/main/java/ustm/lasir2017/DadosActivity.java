package ustm.lasir2017;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class DadosActivity extends AppCompatActivity {
    EditText etNome;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        etNome = (EditText) findViewById(R.id.etNome);
        final RadioGroup rgopcoes = (RadioGroup) findViewById(R.id.rgopcoes);
        Button btVer = (Button) findViewById(R.id.btVer);
        btVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                String nome = (etNome.getText().toString());
                int op = rgopcoes.getCheckedRadioButtonId();
                if (op == R.id.rbMasc)
                    Toast.makeText(DadosActivity.this, nome + " é do sexo Masculino", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(DadosActivity.this, nome + " é do sexo Feminino", Toast.LENGTH_SHORT).show();

            }
        });
    }

}
